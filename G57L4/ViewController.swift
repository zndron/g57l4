//
//  ViewController.swift
//  G57L4
//
//  Created by Andrii on 10/5/17.
//  Copyright © 2017 Andrii. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var inputTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("pew pew")
        let number = Double(inputTextField.text!)!
        print(number)
    }
}
